import numpy as np

# Flags
NO_PIVOT_ALREADY_OPTIMIZED = "already_optimized"
NO_PIVOT_FOUND = "not_found"

# Options
DISPLAY_EXECUTION_TIME = False
OUTPUT_MAX_DECIMAL_PLACES = 7

def read_stdin():
  # Lendo as entradas e colocando nas variaveis correspondentes
  n, m = list(map(int, input().split()))
  c = np.array(list(map(float, input().split())))
  ab = np.array([list(map(float, input().split())) for _ in range(n)])
  a = ab[:, :-1]
  b = ab[:, -1]
  return n, m, c, a, b

# Number formatting for printing
def fn(n):
  return str(f"%.{OUTPUT_MAX_DECIMAL_PLACES}f" % n)

def send_inviable_output(vero_auxiliary):
  print("inviavel")
  print(" ".join(fn(x) for x in vero_auxiliary[0]))

def send_optimal_output(m, auxiliary, vero_auxiliary):
  print("otima")
  print(fn(auxiliary[0, -1]))

  def a(_, row_i): print(fn(auxiliary[row_i, -1]), end=" ")
  def b(_): print(fn(0), end=" ")
  search_pivot_base_column(auxiliary, a, b, columns=list(range(m)))

  print("\n" + " ".join(fn(x) for x in vero_auxiliary[0]))

def send_unlimited_output(m, auxiliary):
  print("ilimitada")

  def a1(_, row_i): print(fn(auxiliary[row_i, -1]), end=" ")
  def b1(_): print(fn(0), end=" ")
  search_pivot_base_column(auxiliary, a1, b1, columns=[i for i in range(m)])
  print()

  # Criando o vetor d
  d = np.zeros(auxiliary.shape[1])

  # Encontrando a coluna totalmente negativa
  is_column_negative = np.all(auxiliary <= 0, axis=0)
  negative_col = np.where(is_column_negative)[0]
  non_negative_cols = np.where(np.bitwise_not(is_column_negative))[0]

  # D = 1 para a coluna toda negativa
  d[negative_col] = 1

  # D = 0 para toda variavel não básica e d_k = -a_li para toda variável básica com 1 na restrição l
  def a2(col_i, row_i): d[col_i] = -auxiliary[row_i, negative_col[0]]
  def b2(col_i): d[col_i] = 0
  search_pivot_base_column(auxiliary, a2, b2, columns=[i for i in non_negative_cols if i < m])
  print(" ".join(fn(x) for x in d[:m]))

def initialize_tableau(n, m, c, a, b):
  # Criando a matriz do tebleau
  tableau = np.zeros((n + 1, n + m + 1))

  # Adicionando o vetor de pesos na primeira linha
  tableau[0, : m] = -c

  # Adicionando a matriz A na esquerda do tableau
  tableau[1 : n + 1, : m] = a

  # Adicionando uma matriz identidade à direita da matriz A
  tableau[1 :, m : -1] = np.eye(n)

  # Adicionando o vetor B na coluna a direita do tableau
  tableau[1 :, -1] = b

  # Multiplicando as linhas com b < 0 por -1
  tableau[np.where(b < 0)[0] + 1] *= -1

  # Retornando a matriz criada
  return tableau

def initialize_vero(n, b):
  # Criando a matriz vero
  vero = np.zeros((n + 1, n))

  # Criando um vetor com -1 onde b < 0 e 1 onde b >= 0
  b_signals = (b >= 0).astype(int)
  b_signals[b_signals == 0] = -1

  # Adicionando o vetor criado anteriormente como uma diagonal na matriz
  vero[1 :, :] = np.diag(b_signals)

  # Retornando a matriz criada
  return vero

def initialize_auxiliary(n, m, b, tableau):
  # Criando a matriz auxiliar
  auxiliary = np.zeros((n + 1, 2 * n + m + 1))

  # Colocando 1 como peso das variáveis auxiliares criadas
  auxiliary[0, n + m : -1] = 1

  # Adicionando o valor do tableu nas primeiras colunas
  auxiliary[1 :, : n + m] = tableau[1 :, : -1]

  # Colocando uma matriz identidade à dirita dos valores do tableau
  auxiliary[1 :, n + m : - 1] = np.eye(n)

  # Colocando o valor absoluto de B na última coluna
  auxiliary[1 :, -1] = np.abs(b)

  # Colocando como peso das variáveis do tableau a soma dos valores daquela coluna
  auxiliary[0, : -1] -= np.sum(auxiliary[1 :, : -1], axis=0)

  # Colocando como peso da última variável como soma dos valores de -abs(B)
  auxiliary[0, -1] = -np.sum(np.abs(b))

  # Retornando a matriz criada
  return auxiliary

def initialize_vero_auxiliary(n, vero):
  # Criando a matriz auxiliar
  vero_auxiliary = np.zeros((n + 1, n))

  # Colocando a matriz vero nas últimas linhas
  vero_auxiliary[1 :, :] = vero[1 :, :]

  # Primeira linha vai ser -soma das colunas
  vero_auxiliary[0, :] = -np.sum(vero_auxiliary[1 :, :], axis=0)

  # Retornando a matriz criada
  return vero_auxiliary

def find_pivot(tableau):
  pivot = None
  min_ratio = None

  # Descobrindo as colunas com primeira linha < 0
  cols_to_search = np.where(tableau[0, : -1] < 0)[0]

  # Checando se existe valor negativo na primeira linha
  if len(cols_to_search) > 0:
    # Se existir, vamos procurar o pivo
    for col_i in cols_to_search:
      # Descobrindo as linhas com valor > 0
      rows_to_search = np.where(tableau[1 :, col_i] > 0)[0] + 1

      # Descobrindo qual o menor
      for row_i in rows_to_search:
        ratio = tableau[row_i, -1] / tableau[row_i, col_i]
        if pivot is None or ratio < min_ratio:
          min_ratio = ratio
          pivot = (row_i, col_i)

      # Se encontramos um pivo, podemos sair do loop
      if min_ratio is not None:
        break
  else:
    # Se não existir coluna com valor < 0, o tableau já está otimizado
    return NO_PIVOT_ALREADY_OPTIMIZED

  if min_ratio is None:
    # Se não foi possível encontrar valor positivo nas linhas com valor < 0
    return NO_PIVOT_FOUND
  else:
    # Retorna o pivo encontrado
    return pivot

def pivot_auxiliaries(auxiliary, vero_auxiliary, pivot):
  # Divide toda a coluna do pivo pelo valor dele
  vero_auxiliary[pivot[0], :] /= auxiliary[pivot]
  auxiliary[pivot[0], :] /= auxiliary[pivot]

  # Atualiza os valores das linhas
  for col_i in range(auxiliary.shape[0]):
    if col_i != pivot[0]:
      pivot_row_val = auxiliary[col_i, pivot[1]]
      if pivot_row_val != 0:
        auxiliary[col_i, :] -= pivot_row_val * auxiliary[pivot[0], :]
        vero_auxiliary[col_i, :] -= pivot_row_val * vero_auxiliary[pivot[0], :]

        # Resolvendo problemas de precisão numérica
        auxiliary[np.abs(auxiliary) < 1e-8] = 0
        vero_auxiliary[np.abs(vero_auxiliary) < 1e-8] = 0

  # Retorna as matrizes atualizadas
  return auxiliary, vero_auxiliary

def search_pivot_base_column(auxiliary, truefunc=None, falsefunc=None, columns=None):
  for col_i in range(auxiliary.shape[1] - 1) if columns is None else columns:
    # Filtro dizendo que a coluna tem que ter somente 0s e 1s e no máximo um 1.
    identity = np.all((auxiliary[1 :, col_i] == 0) | (auxiliary[1 :, col_i] == 1)) and np.sum(auxiliary[1 :, col_i] == 1) <= 1
    
    # Encontrando o "1" na coluna
    base_row_i_list = np.where(auxiliary[:, col_i] == 1)[0]

    # Se tiver o "1" na coluna
    if identity and len(base_row_i_list) > 0 and base_row_i_list[0] > 0:
      if truefunc is not None:
        truefunc(col_i, base_row_i_list[0])
    else:
      if falsefunc is not None:
        falsefunc(col_i)

def main():
  # Lendo as variáveis providas na entrada padrão
  n, m, c, a, b = read_stdin()

  # Inicializando todas as matrizes que serão usadas
  tableau = initialize_tableau(n, m, c, a, b)
  vero = initialize_vero(n, b)
  auxiliary = initialize_auxiliary(n, m, b, tableau)
  vero_auxiliary = initialize_vero_auxiliary(n, vero)

  # Pivoteando a matriz auxiliar até que ela fique otimizada
  pivot = find_pivot(auxiliary)
  while pivot != NO_PIVOT_ALREADY_OPTIMIZED and pivot != NO_PIVOT_FOUND:
    auxiliary, vero_auxiliary = pivot_auxiliaries(auxiliary, vero_auxiliary, pivot)
    pivot = find_pivot(auxiliary)

  # Se o canto superior direito da matriz auxiliar for < 0, o problema é inviável
  if auxiliary[0, -1] < 0:
    send_inviable_output(vero_auxiliary)
    return

  # Atualizando os valores da primeira linha das matrizes auxiliares
  auxiliary[0, : n + m + 1] = tableau[0, :]
  auxiliary[0, n + m + 1 :] = 0
  vero_auxiliary[0, :] = 0

  # Procurando alguma coluna que seja parte da base e pivoteia
  def pivots_line(col_i, row_i):
    vero_auxiliary[0, :] -= auxiliary[0, col_i] * vero_auxiliary[row_i, :]
    auxiliary[0, :] -= auxiliary[0, col_i] * auxiliary[row_i, :]

  search_pivot_base_column(auxiliary, pivots_line)

  # Otimizando a matriz auxiliar
  pivot = find_pivot(auxiliary)
  while pivot != NO_PIVOT_ALREADY_OPTIMIZED and pivot != NO_PIVOT_FOUND:
    auxiliary, vero_auxiliary = pivot_auxiliaries(auxiliary, vero_auxiliary, pivot)
    pivot = find_pivot(auxiliary)

  # Se de fato conseguimos otimizar a auxiliar, temos uma solução ótima
  if pivot == NO_PIVOT_ALREADY_OPTIMIZED:
    send_optimal_output(m, auxiliary, vero_auxiliary)
    return
  
  # Se não encontrou um pivo, temos um problema ilimitado
  if pivot == NO_PIVOT_FOUND:
    send_unlimited_output(m, auxiliary)
    return

def timed_main():
  try:
    import time
    start_time = time.time()
    main()
    end_time = time.time()
    elapsed_ms = (end_time - start_time) * 1000
    print(f"Execution time: {elapsed_ms} ms")
  except:
    main()

if __name__ == "__main__":
  if DISPLAY_EXECUTION_TIME:
    timed_main()
  else:
    main()